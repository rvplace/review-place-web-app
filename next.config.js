/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'images.foody.vn',
        port: '',
        pathname: '/**',
      },
    ],
  },
}

module.exports = nextConfig
