import Image from "next/image";
import styles from "../../styles/Login.module.css";
import Link from "next/link";
import Head from "next/head";
import {useRouter} from "next/router";

function Login() {
    const router = useRouter();

    const clickLogin = async () => {
        await router.push("/user/login");
    };

    const returnHomePage = async () => {
        await router.push("/");
    };

    const callRegister = async (event: any) => {
        event.preventDefault();

        const registerRequest = {
            userName: event.target.userName.value,
            fullName: event.target.fullName.value,
            password: event.target.password.value,
            confirmedPassword: event.target.confirmedPassword.value
        };

        console.log(registerRequest);

        try {
            const url = process.env.NEXT_PUBLIC_GATEWAY_BASE_URL + `/user/account/register`;
            const res = await fetch(url, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(registerRequest)
            });
            const data = await res.json();

            await router.push("/user/login");
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <main className="d-flex flex-column min-vh-100">
            <Head>
                <title>Register</title>
                <meta name="description" content="Generated by create next app"/>
                <link rel="icon" href="../public/favicon.ico"/>
            </Head>

            <section className={["h-100", styles.gradientForm].join(" ")} style={{backgroundColor: "#eee",}}>
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col-xl-10">
                            <div className="card rounded-3 text-black">
                                <div className="row g-0">
                                    <div className="col-lg-6">
                                        <div className="card-body p-md-5 mx-md-4">

                                            <div className="text-center">
                                                <Image
                                                    src={"/image/logo.jpg"}
                                                    width={185} height={120} alt="logo"
                                                    onClick={returnHomePage}>
                                                </Image>
                                                <h4 className="mt-1 mb-5 pb-1">Welcome to Review Place</h4>
                                            </div>

                                            <form onSubmit={callRegister}>
                                                <p style={{
                                                    textAlign: "center"
                                                }}>Please insert your information to create your own account</p>

                                                <div className="form-outline mb-4">
                                                    <label className="form-label"
                                                           htmlFor="userName">Username</label>
                                                    <input type="text" id="userName" className="form-control"
                                                           placeholder="Type your phone number or email address"/>
                                                </div>

                                                <div className="form-outline mb-4">
                                                    <label className="form-label"
                                                           htmlFor="fullName">Full Name</label>
                                                    <input type="text" id="fullName" className="form-control"
                                                           placeholder="Type your full name"/>
                                                </div>

                                                <div className="form-outline mb-4">
                                                    <label className="form-label"
                                                           htmlFor="password">Password</label>
                                                    <input type="password" id="password"
                                                           className="form-control"/>
                                                </div>

                                                <div className="form-outline mb-4">
                                                    <label className="form-label"
                                                           htmlFor="confirmedPassword">Confirm Password</label>
                                                    <input type="password" id="confirmedPassword"
                                                           className="form-control"/>
                                                </div>

                                                <div className="text-center pt-1 mb-5 pb-1">
                                                    <button
                                                        className={["btn btn-primary btn-block fa-lg", styles.gradientCustom2, "mb-3"].join(" ")}
                                                        type="submit"
                                                        style={{
                                                            width: "100%"
                                                        }}>Register
                                                    </button>
                                                </div>

                                                <div className="d-flex align-items-center justify-content-center pb-4">
                                                    {/* eslint-disable-next-line react/no-unescaped-entities */}
                                                    <p className="mb-0 me-2">Already have an account?</p>
                                                    <button type="button" className="btn btn-outline-danger"
                                                            onClick={clickLogin}>Login
                                                    </button>
                                                </div>

                                            </form>

                                        </div>
                                    </div>
                                    <div
                                        className={["col-lg-6 d-flex align-items-center", styles.gradientCustom2].join(" ")}>
                                        <div className="text-white px-3 py-4 p-md-5 mx-md-4">
                                            <h4 className="mb-4">We are more than just a company</h4>
                                            <p className="small mb-0">Lorem ipsum dolor sit amet, consectetur
                                                adipisicing
                                                elit, sed do eiusmod
                                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                                veniam,
                                                quis nostrud
                                                exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                consequat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    )
}

export default Login;