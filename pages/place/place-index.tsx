import Image from "next/image";
import React, {useEffect, useState} from 'react';
import Link from "next/link";

const PlaceIndex = ({filterSearch}: any) => {
    const [groupPlace, setGroupPlace] = useState<any>(null);

    const placeDataDefault = [
        {
            name: "Italian Pizza - Khuất Duy Tiến",
            address: "15 Khuất Duy Tiến, P. Thanh Xuân Bắc, Thanh Xuân, Hà Nội",
            price: "$40.00 - $80.00",
            originalPrice: "$100",
            image: "https://images.foody.vn/res/g91/904681/prof/s750x400/file_restaurant_photo_tj1t_16020-df0f0a84-201007175510.jpg"
        }
    ];

    const placeDataRenderDefault = () => {
        let placeRenderDefaultLet: JSX.Element[] = [];

        placeDataDefault.forEach((item) => {
            placeRenderDefaultLet.push(
                <div className="col mb-5">
                    <div className="card h-100">
                        <div className="badge bg-dark text-white position-absolute"
                             data-style="top: 0.5rem; right: 0.5rem">Sale
                        </div>
                        <Image className="card-img-top" src={item.image} width="450" height="300"
                               alt="..."/>
                        <div className="card-body p-4">
                            <div className="">
                                <h5 className="fw-bolder">{item.name}</h5>
                                <div className="d-flex justify-content-center small text-warning mb-2">
                                    <div className="bi-star-fill"></div>
                                    <div className="bi-star-fill"></div>
                                    <div className="bi-star-fill"></div>
                                    <div className="bi-star-fill"></div>
                                    <div className="bi-star-fill"></div>
                                </div>
                                {item.address} <br/>
                                <span className="text-muted text-decoration-line-through">{item.originalPrice}</span>&nbsp;
                                {item.price}
                            </div>
                        </div>
                        <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div className="text-center"><a className="btn btn-outline-dark mt-auto"
                                                            href="#">Add to cart</a></div>
                        </div>
                    </div>
                </div>
            );
        });
        return placeRenderDefaultLet;
    }

    const [placeData, setPlaceData] = useState<any>(null);

    useEffect(() => {
        getPlaceData();
    }, [filterSearch, groupPlace]);

    const getPlaceData = async () => {
        const url = process.env.NEXT_PUBLIC_GATEWAY_BASE_URL + `/place/api/place?` +
            (groupPlace ? 'groupId=' + groupPlace : '') + new URLSearchParams(filterSearch).toString();

        try {
            const res = await fetch(url, {
                method: "GET",

            });

            const data = await res.json();

            if (data.success === true) {
                setPlaceData(data.data.data);
            }
        } catch (err) {
            console.log(err);
        }
    }

    const placeRender = () => {
        let placeRenderLet: JSX.Element[] = [];

        // @ts-ignore
        placeData.forEach((item) => {
            placeRenderLet.push(
                <div className="col mb-5">
                    <div className="card h-100">
                        <div className="badge bg-dark text-white position-absolute"
                             data-style="top: 0.5rem; right: 0.5rem">Sale
                        </div>
                        <Image className="card-img-top" src={item.avatarFileName} width="450" height="300"
                               alt="..."/>
                        <div className="card-body p-4">
                            <div className="">
                                <h5 className="fw-bolder">{item.name}</h5>
                                <div className="d-flex justify-content-center small text-warning mb-2">
                                    <div className="bi-star-fill"></div>
                                    <div className="bi-star-fill"></div>
                                    <div className="bi-star-fill"></div>
                                    <div className="bi-star-fill"></div>
                                    <div className="bi-star-fill"></div>
                                </div>
                                {item.address} <br/>
                                {item.minPrice} - {item.maxPrice}
                            </div>
                        </div>
                        {/*<div className="card-footer p-4 pt-0 border-top-0 bg-transparent">*/}
                        {/*    <div className="text-center"><a className="btn btn-outline-dark mt-auto"*/}
                        {/*                                    href="#">Add to cart</a></div>*/}
                        {/*</div>*/}
                    </div>
                </div>
            );
        });
        return placeRenderLet;
    }

    return (
        <section className="py-3">
            <div className="container px-4 px-lg-5 mt-5 card" style={{
                paddingLeft: "0 !important",
                paddingRight: "0 !important"
            }}>
                <div className="card-header">
                    <Image src={"/image/giaohang.png"} alt={""} width="50" height="50"></Image>
                    <h4 style={{
                        display: "inline-block",
                        verticalAlign: "middle"
                    }}>&nbsp;&nbsp;&nbsp;Giao tận nơi </h4>

                    <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{
                        display: "inline-block",
                        verticalAlign: "middle"
                    }}>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1"
                                aria-expanded="false" aria-label="Toggle navigation"><span
                            className="navbar-toggler-icon"></span></button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent1">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                                <li className="nav-item"><Link className="nav-link" aria-current="page" href="" id="dealToday" onClick={(e) => {
                                    setGroupPlace(1)
                                }}>Deal hôm nay</Link>
                                </li>
                                <li className="nav-item"><Link className="nav-link" href="" id="all" onClick={(e) => {
                                    setGroupPlace(null)
                                }}>Tất cả</Link></li>
                                <li className="nav-item"><Link className="nav-link" href="" id="food" onClick={(e) => {
                                    setGroupPlace(3)
                                }}>Đồ ăn</Link></li>
                                <li className="nav-item"><Link className="nav-link" href="" id="drink" onClick={(e) => {
                                    setGroupPlace(4)
                                }}>Đồ uống</Link></li>
                                <li className="nav-item"><Link className="nav-link" href="" id="vegetarian" onClick={(e) => {
                                    setGroupPlace(5)
                                }}>Đồ chay</Link></li>
                                <li className="nav-item"><Link className="nav-link" href="" id="cake" onClick={(e) => {
                                    setGroupPlace(6)
                                }}>Bánh kem</Link></li>
                                <li className="nav-item"><Link className="nav-link" href="" id="dessert" onClick={(e) => {
                                    setGroupPlace(7)
                                }}>Tráng miệng</Link></li>
                                <li className="nav-item dropdown">
                                    <Link className="nav-link dropdown-toggle" id="navbarDropdown1" href="#" role="button"
                                          data-bs-toggle="dropdown" aria-expanded="false">Xem thêm</Link>
                                    <ul className="dropdown-menu" aria-labelledby="navbarDropdown1">
                                        <li><Link className="dropdown-item" href="">Homemade</Link></li>
                                        <li><hr className="dropdown-divider"/></li>
                                        <li><Link className="dropdown-item" href="">Vỉa hè</Link></li>
                                        <li><hr className="dropdown-divider"/></li>
                                        <li><Link className="dropdown-item" href="">Pizza/Burger</Link></li>
                                        <li><hr className="dropdown-divider"/></li>
                                        <li><Link className="dropdown-item" href="">Món gà</Link></li>
                                        <li><hr className="dropdown-divider"/></li>
                                        <li><Link className="dropdown-item" href="">Món lẩu</Link></li>
                                        <li><hr className="dropdown-divider"/></li>
                                        <li><Link className="dropdown-item" href="">Khác</Link></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div className="card-body">
                    <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">

                        {placeData && placeData.length > 0 ? placeRender() : placeDataRenderDefault()}

                    </div>
                </div>
            </div>
        </section>
    );
};

export default PlaceIndex;
