import Link from "next/link";
import React from "react";

const NavLeft = () => {
    return (
        <div className="row">
            <div className="col-2">
                <nav className="navbar bg-light">
                    <ul className="nav navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" href="#"> Home </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"> Services </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"> Contact </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"> Blogs </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div className="col-9">
                <div className="container">
                    The Web Content in detail.
                </div>
                <div className="container">
                    <p> The vertical menu can place the left or right side of the web pages. <br/> But the default
                        vertical menu placed on the left side. </p>
                </div>
            </div>
        </div>
    );
};

export default NavLeft;