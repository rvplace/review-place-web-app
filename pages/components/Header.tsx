import {useRouter} from "next/router";
import Link from "next/link";
import Image from "next/image";
import {Dropdown} from "@nextui-org/react";
import React, {useEffect, useState} from "react";

const Header = () => {
    const router = useRouter();

    const clickLogin = async () => {
        await router.push("/user/login");
    };

    const [userInfo, setUserInfo] = useState<any>(null);

    useEffect(() => {
        let userInfo = localStorage.getItem("userInfo");
        console.log(userInfo);
        if (userInfo) {
            setUserInfo(JSON.parse(userInfo));
        } else {
            fetchUserInfo();
        }
    }, []);

    async function fetchUserInfo() {
        const url = process.env.NEXT_PUBLIC_GATEWAY_BASE_URL + `/user/account/login`;

        const token = localStorage.getItem("token");

        if (token) {
            const res = await fetch(url, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + token
                }
            });

            const data = await res.json();

            if (data.success === true) {
                setUserInfo(data);
                localStorage.setItem("userInfo", JSON.stringify(data));
            }
        }
    }

    const loginButton = () => {
        return <div className="d-flex">
            <button className="btn btn-outline-dark" type="button" onClick={clickLogin}>
                Login
            </button>
        </div>
    }

    const logout = async () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
        setUserInfo(null);

        await router.push("/");
    }

    const userInfoButton = () => {
        console.log(userInfo)
        return <div className="d-flex">
            <div className="me-2 dropdown" style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center"
            }}>
                <Dropdown>
                    <Dropdown.Button flat>
                        <strong>{userInfo.data.user.fullName}&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                        <Image src={"/image/avatar.png"} alt={"avatar"} width={25} height={25} style={{
                            verticalAlign: "middle",
                            borderRadius: "50%"
                        }}></Image>
                    </Dropdown.Button>
                    <Dropdown.Menu aria-label="User Profile" selectionMode="single" onSelectionChange={(e) => {
                        // @ts-ignore
                        const { currentKey } = e
                        if (currentKey === "logout") {
                            logout().then(() => {});
                        }
                    }}>
                        <Dropdown.Item key="updateProfile">Update Profile</Dropdown.Item>
                        <Dropdown.Item key="customerService">Customer Service</Dropdown.Item>
                        <Dropdown.Item key="logout" withDivider color="error">Logout</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </div>
        </div>
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{
            height: "50px"
        }}>
            <div className="container px-4 px-lg-5">
                <Link className="navbar-brand" href="/">
                    <Image src={"/image/logo.jpg"} alt={"Logo"} width={80} height={50}></Image>ReviewPlace
                </Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"><span
                    className="navbar-toggler-icon"></span></button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        <li className="nav-item"><Link className="nav-link" aria-current="page"
                                                       href="/">Home</Link>
                        </li>
                        <li className="nav-item"><Link className="nav-link" href="/about">About</Link></li>
                        <li className="nav-item dropdown">
                            <Link className="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button"
                                  data-bs-toggle="dropdown" aria-expanded="false">Shop</Link>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><Link className="dropdown-item" href="#">All Products</Link></li>
                                <li>
                                    <hr className="dropdown-divider"/>
                                </li>
                                <li><Link className="dropdown-item" href="#">Popular Items</Link></li>
                                <li><Link className="dropdown-item" href="#">New Arrivals</Link></li>
                            </ul>
                        </li>
                    </ul>

                    { userInfo ? userInfoButton() : loginButton()  }
                </div>
            </div>

            {/* eslint-disable-next-line @next/next/no-sync-scripts */}
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        </nav>
    )
        ;
};

export default Header;