import {FaSearch} from "react-icons/fa";
import {useEffect, useState} from "react";

const FilterSearch = ({setFilterSearch}: any) => {

    const [listProvince, setListProvince] = useState<any>(null);
    const [listCategory, setListCategory] = useState<any>(null);
    const [searchInputPlace, setSearchInputPlace] = useState<any>(null);

    useEffect(() => {
        getAllProvince();
        getAllCategory();
    }, []);

    const getAllProvince = async () => {
        const url = process.env.NEXT_PUBLIC_GATEWAY_BASE_URL + `/place/api/province`;

        try {
            const res = await fetch(url, {
                method: "GET"
            });

            const data = await res.json();

            if (data.success === true) {
                setListProvince(data.data.data);
            }
        } catch (err) {
            console.log(err);
        }
    }

    const getAllCategory = async () => {
        const url = process.env.NEXT_PUBLIC_GATEWAY_BASE_URL + `/place/api/category`;

        try {
            const res = await fetch(url, {
                method: "GET"
            });

            const data = await res.json();

            if (data.success === true) {
                setListCategory(data.data.data);
            }
        } catch (err) {
            console.log(err);
        }
    }

    const provinceSelection = () => {
        return <div className="col-2">
            <select className="form-select form-select-lg" aria-label=".form-select-lg example"
                    style={{
                        fontSize: '0.8rem',
                    }} onChange={(e) => setFilterSearch(e.target.value, "provinceId")}>
                {
                    listProvince.map((province: { id: number; name: string }) => {
                        return (
                            province.id == 1 ?
                                (<option value={province.id}>{province.name}</option>) :
                                (<option value={province.id}>{province.name}</option>)
                        )
                    })
                }
            </select>
        </div>
    }

    const provinceSelectionDefault = () => {
        return <div className="col-2">
            <select className="form-select form-select-lg" aria-label=".form-select-lg example"
                    style={{
                        fontSize: '0.8rem',
                    }} onChange={(e) => setFilterSearch(e.target.value, "provinceId")}>
                <option value="1">Hà Nội</option>
                <option value="2">Tp. Hồ Chí Minh</option>
                <option value="3">Đà Nẵng</option>
            </select>
        </div>
    }

    const categorySelection = () => {
        return <div className="col-2">
            <select className="form-select form-select-lg" aria-label=".form-select-lg example"
                    style={{
                        fontSize: '0.8rem',
                    }} onChange={(e) => setFilterSearch(e.target.value, "categoryId")}>
                {
                    listCategory.map((category: { id: number; name: string }) => {
                        return (
                            category.id == 1 ?
                                (<option value={category.id}>{category.name}</option>) :
                                (<option value={category.id}>{category.name}</option>)
                        )
                    })
                }
            </select>
        </div>
    }

    const categorySelectionDefault = () => {
        return <div className="col-2">
            <select className="form-select form-select-lg" aria-label=".form-select-lg example"
                    style={{
                        fontSize: '0.8rem',
                    }} onChange={(e) => setFilterSearch(e.target.value, "categoryId")}>
                <option value="1">Ăn uống</option>
                <option value="2">Du lịch</option>
                <option value="3">Cưới hỏi, Hội nghị</option>
            </select>
        </div>
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light nav-light-blue">
            <div className="container px-4 px-lg-5">
                <div className="row" style={{
                    width: "100%"
                }}>
                    {listProvince && listProvince.length > 0 ? provinceSelection() : provinceSelectionDefault()}
                    {listCategory && listCategory.length > 0 ? categorySelection() : categorySelectionDefault()}

                    <div className="col-4">
                        <div className="input-group">
                            <input className="form-control border-end-0 border" type="text"
                                   placeholder="Nhập địa điểm để tìm kiếm"
                                   id="searchPlaceInput"
                                   style={{
                                       fontSize: '0.8rem'
                                   }}
                                   onChange={(e) => setSearchInputPlace(e.target.value)}
                                   onKeyUp={(e) => {
                                       if (e.key == "Enter") {
                                           setFilterSearch(searchInputPlace, "searchPlaceInput")
                                       }
                                   }}
                            >
                            </input>
                            <span className="input-group-append">
                                <button
                                    className="btn btn-outline-secondary bg-white border-start-0 border ms-n3"
                                    type="button"
                                    onClick={(e) => setFilterSearch(searchInputPlace, "searchPlaceInput")}>
                                    <FaSearch/>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default FilterSearch;