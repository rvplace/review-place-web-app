const HomeBanner = () => {
    return (
        <div className="bg-dark py-5" style={{
            backgroundImage: "url('/image/banner.jpg')",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            height: "450px"
        }}>
            <div className="container px-4 px-lg-5" style={{
                marginTop: "120px"
            }}>
                <div className="text-center text-white">
                    <h1 className="display-4 fw-bolder">Review Place</h1>
                    <p className="lead fw-normal text-white-50 mb-0">A place to share moments together</p>
                </div>
            </div>
        </div>
    );
}

export default HomeBanner;